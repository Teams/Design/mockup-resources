# Mockup Resources

![Mockups Resources](README.gif)

Various resources helpful for creating GNOME mockups. See [GNOME User Interface Guidelines](https://developer.gnome.org/hig/) before diving into mockups head first.

We also recommend looking at [OS Mockups](https://gitlab.gnome.org/Teams/Design/os-mockups) and [App Mockups](https://gitlab.gnome.org/Teams/Design/app-mockups) for more reusable assets.